import React,{useEffect,useState}from 'react';
function PresentationForm(){
    const [conferences, setConferences] = useState([]);  

    const [name, setName] = useState('');
    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }

    const [email, setEmail] = useState('');
    const handleEmailChange = (event) => {
        const value = event.target.value;
        setEmail(value);
    }

    const [companyName, setCompanyName] = useState('');
    const handleCompanyNameChange = (event) => {
        const value = event.target.value;
        setCompanyName(value);
    }

    const [title, setTitle] = useState('');
    const handleTitleChange = (event) => {
        const value = event.target.value;
        setTitle(value);
    }

    const [synopsis, setSynopsis] = useState('');
    const handleSyopsisChange = (event) => {
        const value = event.target.value;
        setSynopsis(value);
    }

    const [conference, setConference] = useState('');;
    const handleConferenceChange = (event) => {
        const value = event.target.value;
        setConference(value);
    }


    const fetchData = async () => {
        const url = 'http://localhost:8000/api/conferences/';
        const response = await fetch(url);

        if (response.ok){
            const data = await response.json();
            setConferences(data.conferences)

        }
    }

    useEffect(()=>{
        fetchData();
    }, []);

    const handleSubmit= async (event) =>{
        event.preventDefault();
        const data={};

        data.presenter_name = name; 
        data.presenter_email = email;
        data.company_name = companyName;
        data.title = title;
        data.synopsis = synopsis;
        data.conference = conference;
        console.log(data);

        const select = document.getElementById("conference")
        const presentationUrl = `http://localhost:8000/api/conferences/${select.options[select.selectedIndex].value}/presentations/`
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(presentationUrl, fetchConfig);
        if (response.ok) {
            const newPresentation = await response.json();
            console.log(newPresentation);

            setName('');
            setEmail('');
            setCompanyName('');
            setTitle('');
            setSynopsis('');
            setConference('');
        }
    }





    return(
        <div className ="row">
            <div className ="offset-3 col-6">
            <div className ="shadow p-4 mt-4">
                <h1>Create a new Presentation</h1>
                <form onSubmit={handleSubmit} id="create-presentation-form">
                <div className ="form-floating mb-3">
                    <input onChange={handleNameChange} value={name} placeholder="presenter_name" required type="text" name="presenter_name" id="presenter_name" className ="form-control"/>
                    <label htmlFor ="presenter_name">Presenter name</label>
                </div>
                <div className ="form-floating mb-3">
                    <input onChange={handleEmailChange} value={email} placeholder="presenter_email" required type="email" name="presenter_email" id="presenter_email" className ="form-control"/>
                    <label htmlFor ="presenter_email">Presenter email</label>
                </div>
                <div className ="form-floating mb-3">
                    <input onChange={handleCompanyNameChange} value={companyName} placeholder="company_name" type="text" name="company_name" id="company_name" className ="form-control"/>
                    <label htmlFor ="company_name">Company Name</label>
                </div>
                <div className ="form-floating mb-3">
                    <input onChange={handleTitleChange} value={title} placeholder="title" required type="text" name="title" id="title" className ="form-control"/>
                    <label htmlFor ="title">Title</label>
                </div>
                <div className ="mb-3">
                    <label onChange={handleSyopsisChange} value={synopsis} htmlFor ="synopsis" className ="form-label">Synopsis</label>
                    <textarea className ="form-control" id="synopsis" name="synopsis" rows="5"></textarea>
                </div>
                <div className ="mb-3">
                    <select onChange={handleConferenceChange} value={conference} required name="conference" id="conference" className ="form-select">
                    <option  value="">Choose a conference</option>
                    {conferences.map(conference=>{
                        return(
                            <option key={conference.id} value={conference.id}>
                                {conference.name}
                            </option>
                        )
                    })}
                    </select>
                </div>
                <button className ="btn btn-primary">Create</button>
                </form>
            </div>
            </div>
        </div>
    );
}

export default PresentationForm;
