import React,{useEffect,useState} from 'react';

function ConferenceForm(){

    const [locations, setLocations] = useState([]);  
    const [name, setName] = useState('');
    const [startDate, setStartDate] = useState('');
    const [endDate, setEndDate] = useState('');
    const [description,setDescription] = useState('');
    const [maxPresentations,setMaxPresentations] = useState('');
    const [maxAttendees,setMaxAttendess] = useState('');
    const [location, setLocation] = useState('');


    const handleSubmit= async (event) =>{
        event.preventDefault();
        const data={};

        data.name = name; 
        data.starts = startDate;
        data.ends = endDate;
        data.description = description;
        data.max_presentations = maxPresentations;
        data.max_attendees = maxAttendees;
        data.location=location
        console.log(data);

        const conferenceUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(conferenceUrl, fetchConfig);
        if (response.ok) {
            const newConference = await response.json();
            console.log(newConference);

            setName('');
            setStartDate('');
            setEndDate('');
            setDescription('');
            setMaxPresentations('');
            setMaxAttendess('');
            setLocation('');
        }
    }
    

    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }

    const handleStartDateChange = (event) => {
        const value = event.target.value;
        setStartDate(value);
    }

    const handleEndDateChange = (event) => {
        const value = event.target.value;
        setEndDate(value);
    }

    const handledescriptionChange = (event) => {
        const value = event.target.value;
        setDescription(value);
    }
    
    const handleMaxPresentationsChange = (event) => {
        const value = event.target.value;
        setMaxPresentations(value);
    }

    const handleMaxAttendeesChange = (event) => {
        const value = event.target.value;
        setMaxAttendess(value);
    }

    const handleLocationChange = (event) => {
        const value = event.target.value;
        setLocation(value);
    }



    const fetchData = async () => {
        const url = 'http://localhost:8000/api/locations/';
        const response = await fetch(url);

        if (response.ok){
            const data = await response.json();
            setLocations(data.locations)

        }
    }

    useEffect(()=>{
        fetchData();
    }, []);

    return(
        <div className="row">
            <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>Create a new Conference</h1>
                <form onSubmit={handleSubmit} id="create-conference-form">
                <div className="form-floating mb-3">
                    <input onChange={handleNameChange} placeholder="name" required type="text" name="name" id="name" value={name} className="form-control"/>
                    <label htmlFor="name">Name</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleStartDateChange} placeholder="starts" required type="date" name="starts" id="starts" value={startDate} className="form-control"/>
                    <label htmlFor="starts">Start Date</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleEndDateChange} placeholder="ends" required type="date" name="ends" id="ends"  value={endDate} className="form-control"/>
                    <label htmlFor="ends">End Date</label>
                </div>
                <div className="mb-3">
                    <label htmlFor="description" className="form-label">Description</label>
                    <textarea  onChange={handledescriptionChange} className="form-control" id="description" name="description" value={description} rows="5"></textarea>
                </div>
                <div className="form-floating mb-3">
                    <input  onChange={handleMaxPresentationsChange} placeholder="max_presentations" required type="number" name="max_presentations" value={maxPresentations} id="max_presentations" className="form-control"/>
                    <label htmlFor="max_presentations">Max Presentations</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleMaxAttendeesChange} placeholder="max_attendees" required type="number" name="max_attendees"  value={maxAttendees} id="max_attendees" className="form-control"/>
                    <label htmlFor="max_attendees">Max Attendees</label>
                </div>
                <div className="mb-3">
                    <select onChange={handleLocationChange} required name="location" id="location"  value={location} className="form-select">
                    <option value="">Choose a Location</option>
                    {locations.map(location => {
                    return (
                        <option key={location.id} value={location.id}>
                            {location.name}
                        </option>
                        );
                    })}
                    </select>
                </div>
                <button className="btn btn-primary">Create</button>
                </form>
            </div>
            </div>
        </div>
    );
}




export default ConferenceForm
